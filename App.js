import React, { Component } from 'react';
import { StackNavigator, createStackNavigator } from 'react-navigation';
import { Root } from 'native-base';

import HomePage from './components/HomePage';

const AppNavigator = createStackNavigator(
  {
    Page: { screen: HomePage }
  },
  {
    headerMode: 'none'
  }
);

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });

    this.setState({
      isLoading: false,
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Expo.AppLoading />
      );
    }

    return (
      <Root>
        <AppNavigator />
      </Root>
    );
  }
}
