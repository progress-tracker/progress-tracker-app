import axios from 'axios';

export default class NumericalMetricsService {
  constructor(apiBaseUrl, authUsername, authPassword) {
    this.apiBaseUrl = apiBaseUrl;
    this.authUsername = authUsername;
    this.authPassword = authPassword;
  }

  createMetrics(weight, weightUnits, bodyFatPercentage) {
    var requestBody = {
      weight: weight,
      weightUnits: weightUnits,
      bodyFatPercentage: bodyFatPercentage
    };

    var requestHeaders = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: 'Basic ' + this._getAuthToken()
    }

    return new Promise((resolve, reject) => {
      axios({
        url: this.apiBaseUrl + '/metrics',
        method: 'POST',
        data: requestBody,
        headers: requestHeaders,
        proxy: false
      }).then(response => {
        if (response.status === 201) {
          resolve();
        } else {
          if (response && response.data && response.data.error) {
            reject(response.status + ': ' + response.data.error)
          } else {
            reject('An unexpected error occurred: ' + response);
          }
        }
      }).catch(error => {
        reject(error)
      });
    });

  }

  _getAuthToken() {
    return new Buffer(this.authUsername + ':' + this.authPassword).toString('base64')
  }
}

