import React, { Component } from 'react';
import { PermissionsAndroid } from 'react-native';
import {
  Container, Header, Left, Right, Body,
  Picker, Title, Content, Form, Item,
  Input, Button, Text, Fab, Icon, Toast,
  Root,
} from 'native-base';

import NumericalMetricsService from '../services/NumericalMetricsService';

export default class HomePage extends Component {
  constructor(props) {
    super(props);

    this.numericalMetricsService = this._instantiateNumericalMetricsService();
    this.onWeightUnitChanged = this.onWeightUnitChanged.bind(this);

    this.state = {
      isCameraPermissionGranted: false,
      weightUnit: 0
    };
  }

  _instantiateNumericalMetricsService() {
    var baseUrl = process.env.NUMERICAL_METRICS_BASE_URL;
    var authUsername = process.env.NUMERICAL_METRICS_AUTH_USERNAME;
    var authPassword = process.env.NUMERICAL_METRICS_AUTH_PASSWORD;

    return new NumericalMetricsService(baseUrl, authUsername, authPassword);
  }

  onWeightUnitChanged(value) {
    this.setState({
      weightUnit: value
    });
  }

  async requestCameraAccess() {
    var isGranted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: 'Progress Tracker wants to use your camera',
        message: 'Progress Tracker must have access to your camera to upload progress pictures'
      }
    );

    this.setState({
      isCameraPermissionGranted: isGranted
    });
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#9a9ca0' }}>
        <Header style={{ backgroundColor: 'black' }}>
          <Left />
          <Body>
            <Title>Progress Tracker</Title>
          </Body>
        </Header>
        <Text />
        <Content>
          <Form>
            <Item picker>
              <Picker
                mode="dropdown"
                selectedValue={this.state.weightUnit}
                onValueChange={this.onWeightUnitChanged}
              >
                <Picker.Item label="Lbs" value="key0" />
                <Picker.Item label="Kg" value="key1" />
              </Picker>
            </Item>

            <Item />

            <Item>
              <Input placeholder="Weight" keyboardType="numeric" />
            </Item>

            <Item />

            <Item last>
              <Input placeholder="Body Fat %" keyboardType="numeric" />
            </Item>
          </Form>

          <Text />
          <Text />

          <Button full success onPress={() => Toast.show({
            text: process.env.NUMERICAL_METRICS_BASE_URL,
            buttonText: 'Ok',
            type: 'danger',
            duration: 5000
          })}>
            <Text>Submit</Text>
          </Button>
        </Content>

        <Fab
          containerStyle={{}}
          style={{ backgroundColor: 'black' }}
          position="bottomLeft"
          onPress={() => console.log('Take picture...')}>
          <Icon name="camera" />
        </Fab>
      </Container>
    );
  }
}
